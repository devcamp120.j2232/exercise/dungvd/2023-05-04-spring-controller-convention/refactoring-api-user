package com.devcamp.orderapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "order_code", nullable = false, updatable = false)
    private String orderCode;

    @Column(name = "pizza_size", nullable = false, updatable = true)
    private String pizzaSize;

    @Column(name = "pizza_type", nullable = false, updatable = true)
    private String pizzaType;

    @Column(name = "voucher_code", nullable = true, updatable = true)
    private String voucherCode;

    @Column(name = "price", nullable = true, updatable = true)
    private long price;

    @Column(name = "paid", nullable = true, updatable = true)
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "ngay_cap_nhat", nullable = true, updatable = true)
    private Date ngayCapNhat;   


    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    @JsonBackReference
    private CUser user;


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getOrderCode() {
        return orderCode;
    }


    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }


    public String getPizzaSize() {
        return pizzaSize;
    }


    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }


    public String getPizzaType() {
        return pizzaType;
    }


    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }


    public String getVoucherCode() {
        return voucherCode;
    }


    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }


    public long getPrice() {
        return price;
    }


    public void setPrice(long price) {
        this.price = price;
    }


    public long getPaid() {
        return paid;
    }


    public void setPaid(long paid) {
        this.paid = paid;
    }


    public Date getNgayTao() {
        return ngayTao;
    }


    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }


    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }


    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }


    public CUser getUser() {
        return user;
    }


    public void setUser(CUser user) {
        this.user = user;
    }

    
}
