package com.devcamp.orderapi.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderapi.model.CUser;
import com.devcamp.orderapi.repository.IUserRepository;

@RestController
public class CUserController {
    @Autowired
    IUserRepository pIUserRepository;

    //API load ra toàn bộ user
    @CrossOrigin
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUser() {
        try {
            return new ResponseEntity<>(pIUserRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin user thông qua ID
    @CrossOrigin
    @GetMapping("/users/{userid}")
    public ResponseEntity<CUser> getUserById(@PathVariable("userid") long userid) {
        try {
            Optional<CUser> userData = pIUserRepository.findById(userid);
            if(userData.isPresent()) {
                CUser user = userData.get();
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API tạo mới 1 user 
    @CrossOrigin
    @PostMapping("/users")
    public ResponseEntity<CUser> createUser(@RequestBody CUser pUser) {
        try {
            CUser user = new CUser();
            user.setFullname(pUser.getFullname());
            user.setAddress(pUser.getAddress());
            user.setEmail(pUser.getEmail());
            user.setPhone(pUser.getPhone());
            user.setOrders(pUser.getOrders());
            user.setNgayTao(new Date());
            user.setNgayCapNhat(null);
            CUser savedUser = pIUserRepository.save(user);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API Sửa thông tin 1 user
    @CrossOrigin
    @PutMapping("/users/{userid}")
    public ResponseEntity<CUser> updateUser(@PathVariable("userid") long userid, @RequestBody CUser pUser) {
        try {
            CUser user = pIUserRepository.findById(userid).get();
            user.setFullname(pUser.getFullname());
            user.setAddress(pUser.getAddress());
            user.setEmail(pUser.getEmail());
            user.setPhone(pUser.getPhone());
            user.setOrders(pUser.getOrders());
            user.setNgayCapNhat(new Date());
            CUser savedUser = pIUserRepository.save(user);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //API delete 1 user
    @CrossOrigin
    @DeleteMapping("/user/{userid}")
    public ResponseEntity<CUser> deleteUser(@PathVariable("userid") long userid) {
        try {
            pIUserRepository.deleteById(userid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    //API delete 1 user
    @DeleteMapping("/users")
    public ResponseEntity<CUser> deleteAllUser() {
        try {
            pIUserRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //API tính tổng số user
    @CrossOrigin
    @GetMapping("users/count")
    public ResponseEntity<Long> countUser() {
        try {
            Long count = pIUserRepository.count();
            return new ResponseEntity<>(count, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm check xem user có tồn tại
    @CrossOrigin
    @GetMapping("users/{userid}/check/")
    public ResponseEntity<Boolean> checkUserById(@PathVariable("userid") Long userid) {
        try {
            Boolean isPresent = pIUserRepository.existsById(userid);
            return new ResponseEntity<>(isPresent, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy userr theo trang và size
    @CrossOrigin
    @GetMapping("users5")
    public ResponseEntity<List<CUser>> getFiveUsers(@RequestParam(name = "page", defaultValue = "1") String page, @RequestParam(name = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWith = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CUser> list = new ArrayList<CUser>();
            pIUserRepository.findAll(pageWith).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
